package com.tuuzed.jsonrpc.internal;

import com.tuuzed.jsonrpc.Conn;
import com.tuuzed.jsonrpc.JsonAdapter;
import com.tuuzed.jsonrpc.JsonRpcServer;
import com.tuuzed.jsonrpc.entity.Response;
import com.tuuzed.jsonrpc.internal.util.CloseUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultConn implements Conn {
    private static final Logger logger = Logger.getLogger("DefaultConn");
    @Nullable
    private Map<String, Object> tags;

    static Conn create(@NotNull JsonRpcServer server,
                       @NotNull Socket client,
                       @NotNull JsonAdapter jsonAdapter,
                       @NotNull Charset charset) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream(), charset.name()));
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream(), charset.name()));
        return new DefaultConn(server, client, jsonAdapter, in, out);
    }

    private final JsonRpcServer server;
    private final Socket client;
    private final JsonAdapter jsonAdapter;
    private final BufferedReader in;
    private final BufferedWriter out;

    private DefaultConn(@NotNull JsonRpcServer server,
                        @NotNull Socket client,
                        @NotNull JsonAdapter jsonAdapter,
                        @NotNull BufferedReader in,
                        @NotNull BufferedWriter out) {
        this.server = server;
        this.client = client;
        this.jsonAdapter = jsonAdapter;
        this.in = in;
        this.out = out;
    }


    @Nullable
    @Override
    public String readLine() throws IOException {
        try {
            return in.readLine();
        } catch (IOException e) {
            close();
            CloseUtil.close(this);
            throw e;
        }
    }

    @Override
    public void writeResp(@NotNull Response resp) {
        try {
            writeText(jsonAdapter.toJson(resp));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "writeResp#IOException", e);
            CloseUtil.close(this);
        }
    }


    @Override
    public void setTag(@NotNull String key, Object tag) {
        if (tags == null) tags = new HashMap<>();
        tags.put(key, tag);
    }

    @Override
    public Object getTag(@NotNull String key) {
        return tags == null ? null : tags.get(key);
    }

    @Override
    public void clearTag() {
        if (tags != null) tags.clear();
    }

    @NotNull
    @Override
    public Socket getSocket() {
        return client;
    }

    private void writeText(@NotNull String text) throws IOException {
        out.write(text);
        out.newLine();
        out.flush();
    }


    @Override
    public void close() throws IOException {
        in.close();
        out.close();
        client.close();
        server.notifyConnClose(this);
    }

    @Override
    public String toString() {
        return client.toString();
    }
}
