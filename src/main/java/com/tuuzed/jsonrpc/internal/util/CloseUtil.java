package com.tuuzed.jsonrpc.internal.util;

import java.io.Closeable;

public class CloseUtil {
    public static void close(Closeable... closeables) {
        if (closeables == null) return;
        for (Closeable it : closeables) {
            if (it == null) continue;
            try {
                it.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
