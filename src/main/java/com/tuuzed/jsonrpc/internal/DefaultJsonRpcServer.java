package com.tuuzed.jsonrpc.internal;

import com.tuuzed.jsonrpc.Conn;
import com.tuuzed.jsonrpc.Service;
import com.tuuzed.jsonrpc.JsonAdapter;
import com.tuuzed.jsonrpc.JsonRpcServer;
import com.tuuzed.jsonrpc.entity.Request;
import com.tuuzed.jsonrpc.entity.Response;
import com.tuuzed.jsonrpc.internal.util.CloseUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultJsonRpcServer implements JsonRpcServer {
    private static final Logger logger = Logger.getLogger("DefaultJsonRpcServer");
    private final AtomicBoolean mIsStop;
    private final ServiceRegistry mServiceRegistry;
    private final Charset mCharset;
    private final JsonAdapter mJsonAdapter;
    private final ExecutorService mThreadPool;
    private Callback mCallback;

    public DefaultJsonRpcServer(@NotNull Charset charset, @NotNull JsonAdapter jsonAdapter, int maxThreadCount) {
        mServiceRegistry = new ServiceRegistry();
        mIsStop = new AtomicBoolean(false);
        mCharset = charset;
        mJsonAdapter = jsonAdapter;
        int corePoolSize = Runtime.getRuntime().availableProcessors();
        mThreadPool = new ThreadPoolExecutor(corePoolSize, maxThreadCount,
                60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(),
                new DefaultThreadFactory("JsonRpc"));
    }

    @Override
    public void start(int port, @NotNull final Callback callback) {
        mCallback = callback;
        mThreadPool.execute(() -> {
            try {
                ServerSocket ss = new ServerSocket();
                InetSocketAddress address = new InetSocketAddress("0.0.0.0", port);
                ss.bind(address);
                mIsStop.set(false);
                logger.info("listen 0.0.0.0:" + port);
                logger.info("Services: " + mServiceRegistry.getServiceMethods());
                while (!mIsStop.get()) {
                    final Socket clientConn = ss.accept();
                    handleClientConn(clientConn);
                }
            } catch (IOException e) {
                mCallback.onError(e);
            }
        });
    }

    @Override
    public void stop() {
        mIsStop.set(true);
    }

    @Override
    public JsonRpcServer register(Service... serviceInstances) {
        mServiceRegistry.register(serviceInstances);
        return this;
    }

    @Override
    public JsonRpcServer unregister(Service... serviceInstances) {
        mServiceRegistry.unregister(serviceInstances);
        return this;
    }

    @Override
    public void notifyConnClose(@NotNull Conn conn) {
        Service[] serviceArray;
        synchronized (mServiceRegistry) {
            serviceArray = mServiceRegistry.getServiceArray();
        }
        for (Service service : serviceArray) {
            if (service != null) service.onClose(conn);
        }
    }

    private void handleClientConn(Socket client) {
        mThreadPool.execute(() -> {
            logger.info("client: " + client.toString() + " join in");
            Conn conn = null;
            try {
                client.setKeepAlive(true);
                conn = DefaultConn.create(this, client, mJsonAdapter, mCharset);
                mCallback.onConnect(client);
                for (; ; ) {
                    String rawReq = conn.readLine();
                    logger.info("rawReq: " + rawReq);
                    if (rawReq == null) break;
                    try {
                        Request req = mJsonAdapter.fromJson(rawReq, Request.class);
                        mServiceRegistry.invokeServiceMethod(conn, req);
                    } catch (Exception e) { // JsonSyntaxException
                        conn.writeResp(Response.getProtoErrorResp(null));
                    }
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, "IOException", e);
            } finally {
                mCallback.onDisconnect(client);
                CloseUtil.close(conn);
            }
        });
    }

    static class DefaultThreadFactory implements ThreadFactory {
        private static final AtomicInteger poolNumber = new AtomicInteger(1);
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        DefaultThreadFactory(@NotNull String name) {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup() :
                    Thread.currentThread().getThreadGroup();
            namePrefix = name +
                    "-pool-" +
                    poolNumber.getAndIncrement() +
                    "-thread-";
        }

        public Thread newThread(@NotNull Runnable r) {
            Thread t = new Thread(group, r,
                    namePrefix + threadNumber.getAndIncrement(),
                    0);
            if (t.isDaemon())
                t.setDaemon(false);
            if (t.getPriority() != Thread.NORM_PRIORITY)
                t.setPriority(Thread.NORM_PRIORITY);
            return t;
        }
    }

}
