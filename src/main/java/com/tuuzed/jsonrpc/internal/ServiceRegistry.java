package com.tuuzed.jsonrpc.internal;

import com.tuuzed.jsonrpc.Conn;
import com.tuuzed.jsonrpc.Service;
import com.tuuzed.jsonrpc.annotation.Intercept;
import com.tuuzed.jsonrpc.annotation.ServiceMethod;
import com.tuuzed.jsonrpc.entity.Request;
import com.tuuzed.jsonrpc.entity.Response;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServiceRegistry {
    private static final Logger logger = Logger.getLogger("ServiceRegistry");

    private final Map<String, Service> mServiceInstances = new HashMap<>();
    private final Map<Service, Method> mInterceptServiceMethods = new HashMap<>();
    private final Map<String, Method> mServiceMethods = new HashMap<>();
    private final Map<Method, Method> mInterceptMethods = new HashMap<>();


    void register(Service... serviceInstances) {
        for (Service serviceInstance : serviceInstances) register(serviceInstance);
    }

    void unregister(Service... serviceInstances) {
        for (Service serviceInstance : serviceInstances) unregister(serviceInstance);
    }

    void invokeServiceMethod(@NotNull Conn conn, @NotNull Request req) {
        logger.info(conn.toString() + " => req: " + req);
        Service service = mServiceInstances.get(req.getService());
        if (service == null) {
            logger.log(Level.WARNING, "No service: " + req.getService());
            conn.writeResp(Response.getNoServiceResp(req));
            return;
        }
        Method method = mServiceMethods.get(String.format("%s#%s", req.getService(), req.getMethod()));
        if (method == null) {
            logger.log(Level.WARNING, "No service: " + req.getService() + "#" + req.getMethod());
            conn.writeResp(Response.getNoServiceResp(req));
            return;
        }
        Method interceptServiceMethod = mInterceptServiceMethods.get(service);
        try {
            if (interceptServiceMethod != null) {
                Object result = interceptServiceMethod.invoke(service, conn, req);
                if (result instanceof Boolean && (Boolean) result) return;
            }
            Method interceptMethod = mInterceptMethods.get(method);
            if (interceptMethod != null) {
                Object result = interceptMethod.invoke(service, conn, req);
                if (result instanceof Boolean && (Boolean) result) return;
                return;
            }
            method.invoke(service, conn, req);
        } catch (Exception e) {
            // IllegalAccessException | InvocationTargetException
            logger.log(Level.SEVERE, "invoke exception", e);
            conn.writeResp(Response.getInvokeExceptionResp(req, e));
        }

    }

    @NotNull
    List<String> getServiceMethods() {
        final Set<String> serviceMethods = mServiceMethods.keySet();
        return new ArrayList<>(serviceMethods);
    }

    @NotNull
    Service[] getServiceArray() {
        Service[] serviceArray = new Service[mServiceInstances.size()];
        return mServiceInstances.values().toArray(serviceArray);
    }

    private void register(@NotNull Service serviceInstance) {
        Class<?> serviceClass = serviceInstance.getClass();
        com.tuuzed.jsonrpc.annotation.Service service = serviceClass.getAnnotation(com.tuuzed.jsonrpc.annotation.Service.class);
        if (service == null) return;
        String serviceName = service.value();
        if (serviceName.length() == 0) serviceName = serviceClass.getSimpleName();
        mServiceInstances.put(serviceName, serviceInstance);
        //
        Intercept serviceIntercept = serviceClass.getAnnotation(Intercept.class);
        if (serviceIntercept != null) {
            try {
                Method interceptMethod = serviceClass.getMethod(serviceIntercept.method(), Conn.class, Request.class);
                mInterceptServiceMethods.put(serviceInstance, interceptMethod);
            } catch (NoSuchMethodException e) {
                logger.warning(serviceIntercept.method() + ": " + e.getMessage());
            }
        }
        Method[] methods = serviceClass.getMethods();
        for (Method method : methods) {
            ServiceMethod serviceMethod = method.getAnnotation(ServiceMethod.class);
            if (serviceMethod == null) continue;
            String methodName = serviceMethod.value();
            if (methodName.length() == 0) methodName = method.getName();
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length != 2) continue;
            if (parameterTypes[0] != Conn.class || parameterTypes[1] != Request.class) continue;
            mServiceMethods.put(String.format("%s#%s", serviceName, methodName), method);
            //
            Intercept methodIntercept = method.getAnnotation(Intercept.class);
            if (methodIntercept != null) {
                try {
                    Method interceptMethod = serviceClass.getMethod(methodIntercept.method(), Conn.class, Request.class);
                    mInterceptMethods.put(method, interceptMethod);
                } catch (NoSuchMethodException e) {
                    logger.warning(methodIntercept.method() + ": " + e.getMessage());
                }
            }
        }
    }

    private void unregister(@NotNull Service serviceInstance) {
        Class<?> clazz = serviceInstance.getClass();
        com.tuuzed.jsonrpc.annotation.Service service = clazz.getAnnotation(com.tuuzed.jsonrpc.annotation.Service.class);
        if (service == null) return;
        String serviceName = service.value();
        if (serviceName.length() == 0) serviceName = clazz.getSimpleName();
        mServiceInstances.remove(serviceName);
        List<String> keys = new ArrayList<>();
        for (String key : mServiceMethods.keySet()) {
            if (key.startsWith(serviceName + "#")) {
                keys.add(key);
            }
        }
        for (String key : keys) {
            Method method = mServiceMethods.remove(key);
            if (method != null) {
                mInterceptMethods.remove(method);
            }
        }
    }
}
