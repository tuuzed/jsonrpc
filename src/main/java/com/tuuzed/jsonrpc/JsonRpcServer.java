package com.tuuzed.jsonrpc;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.Socket;

public interface JsonRpcServer {

    interface Callback {
        void onError(@NotNull IOException e);

        void onConnect(@NotNull Socket client);

        void onDisconnect(@NotNull Socket client);
    }

    void start(int port, @NotNull Callback callback);

    void stop();

    JsonRpcServer register(Service... serviceInstances);

    JsonRpcServer unregister(Service... serviceInstances);

    void notifyConnClose(@NotNull Conn conn);
}
