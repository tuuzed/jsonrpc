package com.tuuzed.jsonrpc.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class Request {

    private String id;
    private String service;
    private String method;
    private Map<String, String> params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void putParam(@NotNull String name, String value) {
        if (params == null) params = new HashMap<>();
        params.put(name, value);
    }

    @Nullable
    public String getParam(@NotNull String name) {
        return params == null ? null : params.get(name);
    }

    @Override
    public String toString() {
        return "{" +
                "service='" + service + '\'' +
                ", method='" + method + '\'' +
                ", id='" + id + '\'' +
                ", params=" + params +
                '}';
    }
}
