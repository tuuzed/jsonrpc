package com.tuuzed.jsonrpc.entity;

public class Response {

    public static final int CODE_OK = 200;
    public static final int CODE_FAIL = 400;
    public static final int CODE_NO_SERVICE = 404;
    public static final int CODE_INVOKE_EXCEPTION = 500;
    public static final int CODE_PROTOCOL_ERROR = 800;


    private String id;
    private int code;
    private String msg;
    private Object payload;

    private Response(String id, int code, String msg, Object payload) {
        this.id = id;
        this.code = code;
        this.msg = msg;
        this.payload = payload;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }


    public static Response getProtoErrorResp(Request req) {
        return newResponse(req, Response.CODE_PROTOCOL_ERROR, "Protocol Error");
    }

    public static Response getNoServiceResp(Request req) {
        return newResponse(req, Response.CODE_NO_SERVICE, "No Service: " + req.getService() + "#" + req.getMethod());
    }

    public static Response getInvokeExceptionResp(Request req, Exception e) {
        return newResponse(req, Response.CODE_INVOKE_EXCEPTION, e.toString());
    }


    public static Response newResponse(Request req, int code, String msg) {
        return newResponse(req, code, msg, null);
    }

    public static Response newResponse(Request req, int code, String msg, Object payload) {
        String id = null;
        if (req != null) {
            id = req.getId();
        }
        return new Response(id, code, msg, payload);
    }
}
