package com.tuuzed.jsonrpc;


import com.tuuzed.jsonrpc.entity.Response;
import org.jetbrains.annotations.NotNull;

public abstract class BaseService implements Service {

    @Override
    public void onClose(@NotNull Conn conn) {
    }

    protected void writeResp(@NotNull Conn conn, @NotNull Response resp) {
        conn.writeResp(resp);
    }

}
