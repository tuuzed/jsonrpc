package com.tuuzed.jsonrpc;


import com.tuuzed.jsonrpc.entity.Response;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;

public interface Conn extends Closeable {

    @Nullable
    String readLine() throws IOException;

    void writeResp(@NotNull Response resp);

    void setTag(@NotNull String key, Object tag);

    Object getTag(@NotNull String key);

    void clearTag();

    @NotNull
    Socket getSocket();
}
