package com.tuuzed.jsonrpc;

import org.jetbrains.annotations.NotNull;

public interface JsonAdapter {

    @NotNull
    String toJson(@NotNull Object src);

    @NotNull
    <T> T fromJson(@NotNull String src, @NotNull Class<T> clazz) throws Exception;
}
