package com.tuuzed.jsonrpc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.tuuzed.jsonrpc.internal.DefaultJsonRpcServer;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.Charset;

public final class JsonRpcServerBuilder {
    private Charset charset;
    private JsonAdapter jsonAdapter;
    private int maxThreadSize;

    public JsonRpcServerBuilder setCharset(Charset charset) {
        this.charset = charset;
        return this;
    }

    public JsonRpcServerBuilder setJsonAdapter(JsonAdapter jsonAdapter) {
        this.jsonAdapter = jsonAdapter;
        return this;
    }

    public JsonRpcServerBuilder setMaxThreadSize(int maxThreadSize) {
        this.maxThreadSize = maxThreadSize;
        return this;
    }

    public JsonRpcServer build() {
        if (charset == null) charset = Charset.forName("utf-8");
        if (jsonAdapter == null) {
            jsonAdapter = new JsonAdapter() {
                private final Gson gson = new GsonBuilder()
                        .serializeNulls()
                        .create();

                @NotNull
                @Override
                public String toJson(@NotNull Object src) {
                    return gson.toJson(src);
                }

                @NotNull
                @Override
                public <T> T fromJson(@NotNull String json, @NotNull Class<T> clazz) throws JsonSyntaxException {
                    return gson.fromJson(json, clazz);
                }
            };
        }
        if (maxThreadSize <= 0) maxThreadSize = Integer.MAX_VALUE;
        return new DefaultJsonRpcServer(charset, jsonAdapter, maxThreadSize);
    }
}
