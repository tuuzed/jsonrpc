package com.tuuzed.jsonrpc;

import org.jetbrains.annotations.NotNull;

public interface Service {

    void onClose(@NotNull Conn conn);

}
