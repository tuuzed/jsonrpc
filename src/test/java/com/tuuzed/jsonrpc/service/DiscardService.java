package com.tuuzed.jsonrpc.service;

import com.tuuzed.jsonrpc.BaseService;
import com.tuuzed.jsonrpc.Conn;
import com.tuuzed.jsonrpc.Service;
import com.tuuzed.jsonrpc.annotation.Intercept;
import com.tuuzed.jsonrpc.annotation.ServiceMethod;
import com.tuuzed.jsonrpc.entity.Request;
import com.tuuzed.jsonrpc.entity.Response;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

@com.tuuzed.jsonrpc.annotation.Service("DiscardService")
@Intercept(method = "globalIntercept")
public class DiscardService extends BaseService implements Service {

    private static final Logger logger = Logger.getLogger("DiscardService");

    @ServiceMethod("discard")
    @Intercept(method = "interceptDiscard")
    public void discard(Conn conn, Request req) {
        logger.info(conn.toString() + ", " + req.toString());
    }

    public boolean interceptDiscard(Conn conn, Request req) {
        logger.info("interceptDiscard");
        writeResp(conn, Response.newResponse(req, Response.CODE_OK, "interceptDiscard"));
        return false;
    }

    public boolean globalIntercept(Conn conn, Request req) {
        logger.info("globalIntercept");
        writeResp(conn, Response.newResponse(req, Response.CODE_OK, "globalIntercept"));
        return false;
    }

    @Override
    public void onClose(@NotNull Conn conn) {
        logger.info(Thread.currentThread().getName());
    }
}
