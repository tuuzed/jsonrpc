package com.tuuzed.jsonrpc.service;

import com.tuuzed.jsonrpc.BaseService;
import com.tuuzed.jsonrpc.Conn;
import com.tuuzed.jsonrpc.Service;
import com.tuuzed.jsonrpc.annotation.ServiceMethod;
import com.tuuzed.jsonrpc.entity.Request;
import com.tuuzed.jsonrpc.entity.Response;
import com.tuuzed.jsonrpc.internal.util.SleepUtil;
import com.tuuzed.jsonrpc.util.ThreadPoolUtil;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@com.tuuzed.jsonrpc.annotation.Service("ProducerService")
public class ProducerService extends BaseService implements Service {
    private static final Logger logger = Logger.getLogger("ProducerService");

    @ServiceMethod("subscribe")
    public void subscribe(Conn conn, Request req) {
        logger.info("subscribe: " + Thread.currentThread().getName());
        if (conn.getTag("conn") != null) {
            logger.warning("Already subscribed");
            Response resp = Response.newResponse(req, 400, "Already subscribed");
            conn.writeResp(resp);
            return;
        }
        conn.setTag("conn", conn);
        ThreadPoolUtil.runOnIoThread(() -> {
            int counter = 0;
            while (conn.getTag("conn") != null) {
                Map<String, Object> payload = new HashMap<>(1);
                String msg = "publish message " + ++counter;
                payload.put("publish", msg);
                writeResp(conn, Response.newResponse(req, Response.CODE_OK, "OK", payload));
                SleepUtil.sleep(1000);
                logger.info("publish: " + msg);
            }

        });
        conn.writeResp(Response.newResponse(req, 200, "subscribe success"));
    }

    @ServiceMethod("unsubscribe")
    public void unsubscribe(Conn conn, Request req) {
        conn.clearTag();
        conn.writeResp(Response.newResponse(req, 200, "unsubscribe success"));
    }

    @Override
    public void onClose(@NotNull Conn conn) {
        logger.info("onClose: " + Thread.currentThread().getName());
        conn.clearTag();
    }
}
