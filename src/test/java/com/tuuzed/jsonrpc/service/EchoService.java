package com.tuuzed.jsonrpc.service;

import com.tuuzed.jsonrpc.BaseService;
import com.tuuzed.jsonrpc.Conn;
import com.tuuzed.jsonrpc.Service;
import com.tuuzed.jsonrpc.annotation.ServiceMethod;
import com.tuuzed.jsonrpc.entity.Request;
import com.tuuzed.jsonrpc.entity.Response;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

@com.tuuzed.jsonrpc.annotation.Service("EchoService")
public class EchoService extends BaseService implements Service {
    private static final Logger logger = Logger.getLogger("EchoService");

    @ServiceMethod("echo")
    public void echo(Conn conn, Request req) {
        logger.info(Thread.currentThread().getName() + "#echo");
        writeResp(conn, Response.newResponse(req, 200, "OK", req.getParams()));
    }

    @ServiceMethod("echo2")
    public void echo2(Conn conn, Request req) {
        logger.info(Thread.currentThread().getName() + "#echo2");
        writeResp(conn, Response.newResponse(req, 200, "OK", req.getParams()));
    }

    @Override
    public void onClose(@NotNull Conn conn) {
        logger.info(Thread.currentThread().getName());
    }
}
