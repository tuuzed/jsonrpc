package com.tuuzed.jsonrpc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuuzed.jsonrpc.service.DiscardService;
import com.tuuzed.jsonrpc.service.EchoService;
import com.tuuzed.jsonrpc.service.ProducerService;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.logging.Logger;

public class JsonRpcServerTest {
    private static final Logger log = Logger.getLogger("JsonRpcServerTest");

    @Test
    public void start() throws Exception {
        EchoService echoService = new EchoService();
        ProducerService producerService = new ProducerService();
        DiscardService discardService = new DiscardService();
        JsonRpcServer server = new JsonRpcServerBuilder()
                .setCharset(Charset.forName("gbk"))
                .setJsonAdapter(new JsonAdapter() {
                    private final Gson gson = new GsonBuilder()
                            .serializeNulls()
                            .create();

                    @NotNull
                    @Override
                    public String toJson(@NotNull Object src) {
                        return gson.toJson(src).replace(" ", "");
                    }

                    @NotNull
                    @Override
                    public <T> T fromJson(@NotNull String src, @NotNull Class<T> clazz) {
                        return gson.fromJson(src, clazz);
                    }
                })
                .setMaxThreadSize(20)
                .build();
        server.register(echoService, producerService, discardService)
                .start(9000, new JsonRpcServer.Callback() {
                    @Override
                    public void onError(@NotNull IOException e) {
                        log.severe(e.getMessage());
                    }

                    @Override
                    public void onConnect(@NotNull Socket client) {
                        log.info("onConnect: " + client.toString());
                    }

                    @Override
                    public void onDisconnect(@NotNull Socket client) {
                        log.info("onDisconnect: " + client.toString());
                    }
                });
        @SuppressWarnings("unused")
        int read = System.in.read();
        server.stop();
    }
}